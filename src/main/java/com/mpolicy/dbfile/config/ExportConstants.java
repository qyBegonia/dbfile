package com.mpolicy.dbfile.config;

/**
 * <p> 全局常用变量 </p>
 *
 * @author : zhaoqiuyue
 * @description :
 * @date : 2022/08/17 9:51
 */
public class ExportConstants {

    /**
     * 需要生成word文档的数据库
     */
    public static final String DATABASE = "report";
    /**
     * 生成文件名前缀
     */
    public static final String FILE_NAME = "测试数据库";

    /**
     * 生成文件地址
     */
    public static String FILE_PATH = "/Users/moon/Documents";

}
